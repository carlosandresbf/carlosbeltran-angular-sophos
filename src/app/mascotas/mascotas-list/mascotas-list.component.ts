import { Component, OnInit } from '@angular/core';
import { MascotasService } from '../../services/mascotas/mascotas.service';
import { MascotasModel } from '../../models/mascotas/mascotas-model';
@Component({
  selector: 'app-mascotas-list',
  templateUrl: './mascotas-list.component.html',
  styleUrls: ['./mascotas-list.component.css']
})
export class MascotasListComponent implements OnInit {
// dtOptions: DataTables.Settings = {};
  dtOptions: any = {};
  mascotas: MascotasModel[] = [];

  constructor(private __mascotasService: MascotasService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      responsive: true,
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel'
               ]

    };

    this.mascotas = this.__mascotasService.list();
  }


}
