import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MascotasService } from '../../services/mascotas/mascotas.service';
import { MascotasModel } from '../../models/mascotas/mascotas-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mascotas-create',
  templateUrl: './mascotas-create.component.html',
  styleUrls: ['./mascotas-create.component.css']
})
export class MascotasCreateComponent implements OnInit {
  mascotas: any;


  constructor( private _ActivatedRoute: ActivatedRoute, private _mascotasService: MascotasService, private _Router: Router) {

    this.mascotas = {
      id: 0,
      nombre: '',
      raza: '',
      edad: 0,
      propietario: '',
      foto: '',
      nacimiento: ''
    };


  }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    this._mascotasService.create(f.value);
    this._Router.navigate(['/mascotas']);
  }

}
