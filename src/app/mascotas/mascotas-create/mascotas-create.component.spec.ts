import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MascotasCreateComponent } from './mascotas-create.component';

describe('MascotasCreateComponent', () => {
  let component: MascotasCreateComponent;
  let fixture: ComponentFixture<MascotasCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MascotasCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MascotasCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
