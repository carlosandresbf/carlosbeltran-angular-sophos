import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavigationComponent } from './template/navigation/navigation.component';
import { HeaderComponent } from './template/header/header.component';
import { FootComponent } from './template/foot/foot.component';
import { MascotasCreateComponent } from './mascotas/mascotas-create/mascotas-create.component';
import { MascotasListComponent } from './mascotas/mascotas-list/mascotas-list.component';
import { MyRoutes } from './routes/app.MyRoutes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MascotasService } from './services/mascotas/mascotas.service';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,
    FootComponent,
    MascotasCreateComponent,
    MascotasListComponent,
  ],
  imports: [
    BrowserModule,
    MyRoutes,
    NgbModule,
    DataTablesModule,
    FormsModule,
    AngularFontAwesomeModule
  ],
  providers: [MascotasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
