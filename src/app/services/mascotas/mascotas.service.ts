import { Injectable } from '@angular/core';
import 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MascotasService {

  constructor( ) {

  }

  public list() {

    if (localStorage.getItem('data_mascotas') === null) {
      return [];
    } else {
      return  JSON.parse(localStorage.getItem('data_mascotas'));
    }
  }

  public create(inputData) {
    let data: any = [];
    if (localStorage.getItem('data_mascotas') === null) {
    } else {
      const currentData = JSON.parse(localStorage.getItem('data_mascotas'));
      console.log(currentData);
      data = currentData;
    }
    data.push(inputData);
    localStorage.setItem('data_mascotas', JSON.stringify(data));
    return true;

  }

}
