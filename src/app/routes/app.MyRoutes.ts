import {Routes, RouterModule} from '@angular/router';
import { MascotasCreateComponent } from '../mascotas/mascotas-create/mascotas-create.component';
import { MascotasListComponent } from '../mascotas/mascotas-list/mascotas-list.component';

const ROUTES: Routes = [
  {path: 'mascotas', component: MascotasListComponent},
  {path: 'mascotas/new', component: MascotasCreateComponent},
  {path: 'mascotas/:id/edit', component: MascotasCreateComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'mascotas'},
];


export const MyRoutes = RouterModule.forRoot(ROUTES);
