export class MascotasModel {
  id: number;
  nombre: string;
  raza: string;
  edad: number;
  propietario: string;
  foto: string;
  nacimiento: string;
}
